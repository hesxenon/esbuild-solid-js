import * as Solid from "solid-js";
import { render } from "solid-js/web";
import styles from "./index.module.css";
console.log(styles);

const App = () => {
  const [count, setCount] = Solid.createSignal(0);

  const timer = setInterval(() => setCount((current) => current + 1), 1000);
  Solid.onCleanup(() => clearInterval(timer));

  return <div class={styles.App}>{count()}</div>;
};

const root = document.getElementById("root");
if (root == null) {
  throw new Error("could not find root element!");
}
render(App, root);
