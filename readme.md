# Why does this project exist
I wanted a barebones example of how to use solid-js with esbuild and typescript.

# Requirements/Preset
* must be compatible with [typescript project references](https://www.typescriptlang.org/docs/handbook/project-references.html).
* must _fully_ preserve sourcemaps

## Why not use a typescript loader and skip the tsc step?
Because of said project references.
* none of the typescript loaders I've encountered so far had sufficient support for this.
* only keeping the transpilation result in RAM eliminates most of the benefits of said project references.

# A note about project references
I like to work in monorepos and still have my code split enough so I can publish individual packages. I won't go into the details of modern monorepos here, but let's just imagine we have 3 packages with a dependency like
```
A
|-B
|-C
```
Now if I change something in `B` I don't want `C` to recompile, so just managing all 3 packages in one typescript config is not feasible. This is the exact problem that project references solve, among other things.

# Benefits for the workflow
Just running `tsc -b -w` while developing means that subsequent calls to `tsc -b` will complete pretty fast. Fast enough to put them in a pre-commit hook => No broken commits anymore, no bug tracking through states that don't even compile!

Oh, and just because you quit the watch process doesn't mean typescript has to recompile everything when you start again the next morning :)

So when you run `npm start` you'll not only have relatively low recompilation times, you'll also have the benefits above.

# Roadmap
* showcase css-modules
Tiny project, tiny roadmap.
