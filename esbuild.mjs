import * as Esbuild from "esbuild";
import * as Path from "path";
import cssmodules from "esbuild-plugin-simple-css-modules";
import solid from "esbuild-plugin-solid-js";

const containsFlags = (...flags) =>
  flags.some((flag) => process.argv.includes(flag));

const buildOptions = {
  entryPoints: {
    app: "./build/index.jsx",
  },
  outdir: "public/bundle",
  target: "es2020",
  format: "esm",
  bundle: true,
  metafile: containsFlags("--analyze"),
  sourcemap: containsFlags("--dev"),
  minify: containsFlags("--minify"),
  platform: "browser",
  plugins: [
    solid,
    {
      name: "css-from-src",
      setup(build) {
        const base = process.cwd();
        build.onResolve({ filter: /\.css$/ }, (args) => {
          const path = Path.resolve(
            `${Path.relative(base, args.resolveDir).replace("build", "src")}/${
              args.path
            }`
          );
          return {
            path,
          };
        });
      },
    },
    cssmodules(),
  ],
};

await (containsFlags("--serve")
  ? Esbuild.serve(
      {
        servedir: "public",
        port: 8080,
      },
      buildOptions
    )
  : Esbuild.build(buildOptions).then(async (result) => {
      if (result.metafile != null) {
        console.log(await Esbuild.analyzeMetafile(result.metafile));
      }
      return result;
    }));
